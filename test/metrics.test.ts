import { expect } from "chai";
import { Metric, MetricsHandler } from "../src/controllers/metrics";
import { LevelDB } from "../src/controllers/leveldb";

const dbPath: string = "./db-test/metrics";
var dbMet: MetricsHandler;

describe("Metrics", function() {
    before((done) => {
        LevelDB.clear(dbPath).then(() => {
            dbMet = new MetricsHandler(dbPath);
            console.log("Database opened");
            done();
        }).catch(done);
    });

    after(function(done) {
        dbMet.close().then(() => {
            console.log("Database closed");
            done();
        }).catch(done);
        
    });

    describe("#get", function() {
        it("should get empty array on non existing group", function(done) {
            dbMet.get("0", function(err: Error | null, result?: Metric[]) {
                expect(err).to.be.null;
                expect(result).to.not.be.undefined;
                expect(result).to.be.empty;
                done();
            });
        });
    });

    describe("#save", function() {
        it("should add data to the db", function(done) {
            dbMet.save("6", [new Metric("1234", 42)], (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    if (result) {
                        expect(result[0].timestamp).to.be.equal("1234");
                        expect(result[0].value).to.be.equal(42);
                    } else {
                        throw new Error("No result");
                    }
                    done();
                });
            });
        });
        it("should update data ", function(done) {
            dbMet.save("6", [new Metric("1234", 32)], (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    if (result) {
                        expect(result[0].timestamp).to.be.equal("1234");
                        expect(result[0].value).to.be.equal(32);
                    } else {
                        throw new Error("No result");
                    }
                    done();
                });
            });
        });
        it("should add another data (same id)", function(done) {
            dbMet.save("6", [new Metric("1300", 40)], (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    if (result) {
                        expect(result[1].timestamp).to.be.equal("1300");
                        expect(result[1].value).to.be.equal(40);
                    } else {
                        throw new Error("No result");
                    }
                    done();
                });
            });
        });
    });

    describe("#delete", () => {
        it("should delete the previous data", done => {
            dbMet.deleteByTimestamp("6", "1300", (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    if (result) {
                        expect(result[0].timestamp).to.be.equal("1234");
                        expect(result[0].value).to.be.equal(32);
                    } else {
                        throw new Error("No result");
                    }
                    done();
                });
            });
        });

        it("should try to delete and don't throw error", done => {
            dbMet.deleteById("6", (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    expect(result).to.be.empty;
                    done();
                });
            });
        });
    });
});
