import request from "supertest";
import { expect } from "chai";
import { LevelDB } from "../src/controllers/leveldb";

if (process.env.NODE_ENV !== "TEST") {
    console.log("Bad env, database alter possible => no request tests");
} else {
    let app, server;
    describe("Server test", () => {
        before(function(done) {
            this.timeout(10000);
            LevelDB.clear("./db-test/users")
                .then(() => {
                    LevelDB.clear("./db-test/metrics")
                        .then(() => {
                            app = require("../src/server").app;
                            server = require("../src/server").server;
                            done();
                        })
                        .catch(done);
                })
                .catch(done);
        });
        after(done => {
            server.closeServer(() => {
                done();
            });
        });

        it("test home", done => {
            request(app)
                .get("/")
                .expect(200)
                .then(response => {
                    expect(response.body).to.exist;
                    done();
                })
                .catch(done);
        });

        it("test 404", done => {
            request(app)
                .get("/michel")
                .expect(404)
                .then(response => {
                    expect(response.body).to.exist;
                    expect(response.redirect).to.be.false;
                    done();
                })
                .catch(done);
        });
        it("test user", done => {
            const user = {
                username: "jean",
                email: "jean@gamil.com",
                password: "password",
            };
            request(app)
                .post("/user")
                .send(user)
                .expect(302)
                .then(response => {
                    expect(response.redirect).to.be.true;
                    request(app).get("/").expect(200).then(response => {
                        done();
                    }).catch(done);
                })
                .catch(done);
        });
    });
}
