import { expect } from "chai";
import { UserHandler, User } from "../src/controllers/user";
import { LevelDB } from "../src/controllers/leveldb";

const dbPath: string = "./db-test/users";
var dbUser: UserHandler;

describe("User", function() {
    before(done => {
        LevelDB.clear(dbPath)
            .then(() => {
                dbUser = new UserHandler(dbPath);
                console.log("Database opened");
                done();
            })
            .catch(done);
    });

    after(function(done) {
        dbUser.close();
        console.log("Database closed");
        done();
    });

    describe("#get", function() {
        it("should get undefined on non existing user", function(done) {
            dbUser.get("jack", function(err: Error | null, result?: User) {
                expect(err).to.be.null;
                expect(result).to.be.undefined;
                done();
            });
        });
    });

    describe("#save", function() {
        it("should add data to the db", function(done) {
            dbUser.save(
                new User("jack", "jack@gmail.com", "password"),
                (err: Error | null) => {
                    expect(err).to.be.null;

                    dbUser.get("jack", function(
                        err: Error | null,
                        result?: User
                    ) {
                        expect(err).to.be.null;
                        expect(result).to.not.be.undefined;
                        if (result) {
                            expect(result.username).to.be.equal("jack");
                            expect(result.email).to.be.equal("jack@gmail.com");
                            expect(result.validatePassword("password")).to.be
                                .true;
                        }
                        done();
                    });
                }
            );
        });
        it("should update data: email", function(done) {
            dbUser.get("jack", function(err: Error | null, result?: User) {
                if (err) done(err);
                if (result === undefined)
                    done(new Error("result undefined and no error WTF"));
                else {
                    result.email = "jack@yahoo.com";
                    dbUser.save(result, (err: Error | null) => {
                        expect(err).to.be.null;
                        dbUser.get("jack", function(
                            err: Error | null,
                            result?: User
                        ) {
                            expect(err).to.be.null;
                            expect(result).to.not.be.undefined;
                            if (result) {
                                expect(result.username).to.be.equal("jack");
                                expect(result.email).to.be.equal(
                                    "jack@yahoo.com"
                                );
                                expect(result.validatePassword("password")).to
                                    .be.true;
                            }
                            done();
                        });
                    });
                }
            });
        });

        it("should update data: password", function(done) {
            dbUser.get("jack", function(err: Error | null, result?: User) {
                if (err) done(err);
                if (result === undefined)
                    done(new Error("result undefined and no error WTF"));
                else {
                    result.setPassword("coucou");
                    dbUser.save(result, (err: Error | null) => {
                        expect(err).to.be.null;
                        dbUser.get("jack", function(
                            err: Error | null,
                            result?: User
                        ) {
                            expect(err).to.be.null;
                            expect(result).to.not.be.undefined;
                            if (result) {
                                expect(result.username).to.be.equal("jack");
                                expect(result.email).to.be.equal(
                                    "jack@yahoo.com"
                                );
                                expect(result.validatePassword("password")).to
                                    .be.false;
                                expect(result.validatePassword("coucou")).to.be
                                    .true;
                            }
                            done();
                        });
                    });
                }
            });
        });
    });

    describe("#delete", () => {
        it("should delete the previous data", done => {
            dbUser.delete("jack", (err: Error | null) => {
                expect(err).to.be.null;

                dbUser.get("jack", function(err: Error | null) {
                    expect(err).to.be.null;
                    done();
                });
            });
        });

        it("should try to delete and don't throw error", done => {
            dbUser.delete("jack", (err: Error | null) => {
                expect(err).to.be.null;

                dbUser.get("jack", function(err: Error | null) {
                    expect(err).to.be.null;
                    done();
                });
            });
        });
    });
});
