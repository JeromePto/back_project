"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var leveldb_1 = require("./leveldb");
var bcrypt_1 = __importDefault(require("bcrypt"));
var User = /** @class */ (function () {
    function User(username, email, password, passwordHashed) {
        if (password === void 0) { password = ""; }
        if (passwordHashed === void 0) { passwordHashed = false; }
        this.password = "";
        this.username = username;
        this.email = email;
        if (!passwordHashed) {
            this.setPassword(password);
        }
        else {
            this.password = password;
        }
    }
    User.fromDb = function (username, value) {
        var _a = value.split(":"), password = _a[0], email = _a[1];
        return new User(username, email, password, true);
    };
    User.prototype.setPassword = function (toSet) {
        // Hash and set password
        this.password = bcrypt_1.default.hashSync(toSet, 10);
    };
    User.prototype.getPassword = function () {
        return this.password;
    };
    User.prototype.validatePassword = function (toValidate) {
        return bcrypt_1.default.compareSync(toValidate, this.getPassword());
    };
    return User;
}());
exports.User = User;
var UserHandler = /** @class */ (function () {
    function UserHandler(path) {
        this.db = leveldb_1.LevelDB.open(path);
    }
    UserHandler.prototype.close = function () {
        this.db.close();
    };
    UserHandler.prototype.get = function (username, callback) {
        this.db.get("user:" + username, function (err, data) {
            if (err) {
                if (err.notFound) {
                    return callback(null);
                }
                return callback(err);
            }
            else if (data === undefined)
                callback(null, data);
            callback(null, User.fromDb(username, data));
        });
    };
    UserHandler.prototype.save = function (user, callback) {
        // dev console.log("save: ", user);
        this.db.put("user:" + user.username, user.getPassword() + ":" + user.email, function (err) {
            if (err)
                callback(err);
            return callback(null);
        });
    };
    UserHandler.prototype.delete = function (username, callback) {
        this.db.del("user:" + username, function (err) {
            if (err)
                return callback(err);
            return callback(null);
        });
    };
    return UserHandler;
}());
exports.UserHandler = UserHandler;
