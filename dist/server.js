"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var fs_1 = __importDefault(require("fs"));
var https_1 = __importDefault(require("https"));
var bodyparser = require("body-parser");
var morgan_1 = __importDefault(require("morgan"));
var session = require("express-session");
var level_session_store_1 = __importDefault(require("level-session-store"));
var user_1 = __importDefault(require("./routes/user"));
var auth_1 = __importDefault(require("./routes/auth"));
var metrics_1 = __importDefault(require("./routes/metrics"));
var metrics_from_1 = __importDefault(require("./routes/metrics_from"));
var app = express();
exports.app = app;
var LevelStore = level_session_store_1.default(session);
var port = process.env.PORT || "8080";
app.use(express.static(path.join(__dirname, "/../public")));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.set("views", __dirname + "/../views");
app.set("view engine", "ejs");
// for dev
// LevelDB.clear("./db/users");
app.use(morgan_1.default("dev"));
app.use(session({
    secret: "mon secret",
    store: new LevelStore("./db/sessions"),
    resave: true,
    saveUninitialized: true,
}));
app.get("/dev/req", function (req, res) {
    res.send(req.session);
});
// for dev
// import { User } from "./user";
// dbUser.save(new User("aaa", "aaa@aaa.fr", "aaa"), err => {
//     if (err) {
//         throw err;
//     }
// });
app.use(auth_1.default);
app.use("/user", user_1.default);
app.use(metrics_from_1.default);
app.use("/metrics", metrics_1.default);
app.get("/", function (req, res) {
    res.render("home", { loggedIn: req.session.loggedIn });
});
app.use(function (req, res, next) {
    res.status(404).send("<h1>Page not found</h1>");
});
var server;
exports.server = server;
if (process.env.SECURE === "1") {
    exports.server = server = https_1.default
        .createServer({
        key: fs_1.default.readFileSync("secrets/server.key"),
        cert: fs_1.default.readFileSync("secrets/server.cert"),
    }, app)
        .listen(port, function () {
        console.log("Server is running on https://localhost:" + port);
    });
}
else {
    exports.server = server = app.listen(port, function (err) {
        if (err)
            throw err;
        console.log("Server is running on http://localhost:" + port);
    });
}
