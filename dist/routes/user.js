"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var user_1 = require("../controllers/user");
// import { dbUser } from "../server";
var dbInstances_1 = require("../utils/dbInstances");
var router = express_1.default.Router();
router.post("/", function (req, res, next) {
    dbInstances_1.dbUser.get(req.body.username, function (err, result) {
        if (err || result !== undefined) {
            res.status(409).send("user already exists");
        }
        else {
            var current_1 = new user_1.User(req.body.username, req.body.email, req.body.password);
            dbInstances_1.dbUser.save(current_1, function (err) {
                if (err)
                    next(err);
                else {
                    req.session.loggedIn = true;
                    req.session.user = current_1;
                    res.redirect("/");
                }
            });
        }
    });
});
router.get("/:username", function (req, res, next) {
    dbInstances_1.dbUser.get(req.params.username, function (err, result) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        }
        else
            res.status(200).json(result);
    });
});
router.delete("/:username", function (req, res, next) {
    dbInstances_1.dbUser.get(req.params.username, function (err, result) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        }
        else {
            dbInstances_1.dbUser.delete(req.params.username, function (err) {
                if (err) {
                    return next(err);
                }
                else {
                    if (req.session.loggedIn === true &&
                        req.params.username === req.session.user.username) {
                        delete req.session.loggedIn;
                        delete req.session.user;
                    }
                    res.status(200).send("user deleted");
                }
            });
        }
    });
});
exports.default = router;
