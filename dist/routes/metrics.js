"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
// import { dbMet } from "../server";
var authCheck_1 = require("../utils/authCheck");
var dbInstances_1 = require("../utils/dbInstances");
var router = express_1.default.Router();
router.get("/", authCheck_1.authCheck, function (req, res) {
    dbInstances_1.dbMet.get(req.session.user.username, function (err, result) {
        if (err)
            throw err;
        res.json(result);
    });
});
// for dev
router.get("/all", function (req, res) {
    dbInstances_1.dbMet.getAll(function (err, result) {
        if (err)
            throw err;
        res.json(result);
    });
});
router.post("/", authCheck_1.authCheck, function (req, res) {
    dbInstances_1.dbMet.save(req.session.user.username, [req.body], function (err) {
        if (err)
            throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metrics added" });
    });
});
router.post("/delete-one", authCheck_1.authCheck, function (req, res) {
    dbInstances_1.dbMet.deleteByTimestamp(req.session.user.username, req.body.timestamp, function (err) {
        console.log(err);
        if (err)
            throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metric deleted" });
    });
});
router.post("/delete", authCheck_1.authCheck, function (req, res) {
    dbInstances_1.dbMet.deleteById(req.session.user.username, function (err) {
        if (err)
            throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metrics deleted" });
    });
});
exports.default = router;
