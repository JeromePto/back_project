"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var d3node_linechart_1 = __importDefault(require("d3node-linechart"));
// import { dbMet } from "../server";
var authCheck_1 = require("../utils/authCheck");
var dbInstances_1 = require("../utils/dbInstances");
var router = express_1.default.Router();
router.get("/add-metrics", authCheck_1.authCheck, function (req, res) {
    res.render("add-metrics", { loggedIn: req.session.loggedIn });
});
router.get("/del-metrics", authCheck_1.authCheck, function (req, res) {
    res.render("del-metrics", { loggedIn: req.session.loggedIn });
});
router.get("/get-metrics", authCheck_1.authCheck, function (req, res) {
    var data = new Array();
    dbInstances_1.dbMet.get(req.session.user.username, function (err, result) {
        if (err)
            throw err;
        result.forEach(function (element) {
            data.push({ key: element.timestamp, value: element.value });
        });
        var svg = d3node_linechart_1.default({ data: data, isCurve: false }).svgString();
        res.render("./graph", { loggedIn: req.session.loggedIn, graph: svg });
    });
});
exports.default = router;
