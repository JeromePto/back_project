"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
// import { dbUser } from "../server";
var dbInstances_1 = require("../utils/dbInstances");
var router = express_1.default.Router();
router.get("/login", function (req, res) {
    if (req.session.loggedIn) {
        res.redirect("/");
    }
    else {
        res.render("login", { loggedIn: req.session.loggedIn });
    }
});
router.get("/signup", function (req, res) {
    if (req.session.loggedIn) {
        res.redirect("/");
    }
    else {
        res.render("signup", { loggedIn: req.session.loggedIn });
    }
});
router.get("/logout", function (req, res) {
    if (req.session.loggedIn) {
        delete req.session.loggedIn;
        delete req.session.user;
        res.redirect("/");
    }
    else {
        res.redirect("/");
    }
});
router.post("/login", function (req, res, next) {
    dbInstances_1.dbUser.get(req.body.username, function (err, result) {
        if (err)
            return next(err);
        if (result === undefined ||
            !result.validatePassword(req.body.password)) {
            res.redirect("/login");
        }
        else {
            req.session.loggedIn = true;
            req.session.user = result;
            res.redirect("/");
        }
    });
});
exports.default = router;
