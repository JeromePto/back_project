"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authCheck = function (req, res, next) {
    if (req.session.loggedIn) {
        next();
    }
    else {
        res.redirect("/");
    }
};
