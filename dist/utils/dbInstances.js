"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var metrics_1 = require("../controllers/metrics");
var user_1 = require("../controllers/user");
exports.dbMet = new metrics_1.MetricsHandler("./db/metrics");
exports.dbUser = new user_1.UserHandler("./db/users");
