FROM node:12.13.0
WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .