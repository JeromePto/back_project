[![pipeline status](https://gitlab.com/JeromePto/back_project/badges/master/pipeline.svg)](https://gitlab.com/JeromePto/back_project/commits/master)
[![coverage report](https://gitlab.com/JeromePto/back_project/badges/master/coverage.svg)](https://gitlab.com/JeromePto/back_project/commits/master)

**Name:** Jérôme Petot  

**Title**: Final project of node.js and DevOps  

**Introduction:** A project to discover test and middleware
## Run instruction
To install : `npm install`  
To populate : `npm run populate`  
To run : `npm start`  
To test : `npm run test`  

## Run instruction docker
To launch : `docker-compose up`  
Populate : `docker exec -d node_app npm run docker:populate`  
Test : `docker exec -it node_app npm run test`


# Routes and parameters

| Method | Route | Description | Parameters
| - | - | - | - |
| GET | / | The home page with the menu when logged
| GET | /login | The login page |  |  |
| POST | /login | Login the user | username, password
| GET | /logout | Automatic logout
| GET | /signup | The signup page
| POST | /user | Create an account | username, email, password
| GET | /user/me | Info of the user in JSON
| GET | /add-metrics | The page with a form to add metrics
| GET | /del-metrics | The page with a form to delete metrics
| GET | /get-metrics | The graph of metrics
| GET | /metrics | All the metrics of an user in JSON
| POST | /metrics | Add a new metric | timestamp, value
| POST | /metrics/delete-one | Delete a metric | timestamp
| POST | /metrics/delete | Delete all metrics of an user

