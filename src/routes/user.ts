import express from "express";

import { User } from "../controllers/user";
// import { dbUser } from "../server";

import { dbUser } from "../utils/dbInstances";
import { authCheck } from "../utils/authCheck"

const router = express.Router();

router.post("/", (req: any, res: any, next: any) => {
    dbUser.get(req.body.username, function(err: Error | null, result?: User) {
        if (err || result !== undefined) {
            res.status(409).send("user already exists");
        } else {
            let current = new User(req.body.username, req.body.email, req.body.password)
            dbUser.save(
                current,
                function(err: Error | null) {
                    if (err) next(err);
                    else {
                        req.session.loggedIn = true;
                        req.session.user = current;
                        res.redirect("/");
                    }
                }
            );
        }
    });
});

router.get("/me", authCheck, (req: any, res: any, next: any) => {
    dbUser.get(req.session.user.username, function(err: Error | null, result?: User) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        } else res.status(200).json(result);
    });
});

// router.delete("/:username", (req: any, res: any, next: any) => {
//     dbUser.get(req.params.username, (err: Error | null, result?: User) => {
//         if (err || result === undefined) {
//             res.status(404).send("user not found");
//         } else {
//             dbUser.delete(req.params.username, (err: Error | null) => {
//                 if (err) {
//                     return next(err);
//                 } else {
//                     if (
//                         req.session.loggedIn === true &&
//                         req.params.username === req.session.user.username
//                     ) {
//                         delete req.session.loggedIn;
//                         delete req.session.user;
//                     }
//                     res.status(200).send("user deleted");
//                 }
//             });
//         }
//     });
// });

export default router;
