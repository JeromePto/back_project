import express from "express";

// import { dbMet } from "../server";
import { authCheck } from "../utils/authCheck"

import { dbMet } from "../utils/dbInstances";

const router = express.Router();

router.get("/", authCheck, (req: any, res: any) => {
    dbMet.get(req.session.user.username, (err: Error | null, result?: any) => {
        if (err) throw err;
        res.json(result);
    });
});


// for dev
// router.get("/all", (req: any, res: any) => {
//     dbMet.getAll((err: Error | null, result?: any) => {
//         if (err) throw err;
//         res.json(result);
//     });
// });

router.post("/", authCheck, (req: any, res: any) => {
    dbMet.save(req.session.user.username, [req.body], (err: Error | null) => {
        if (err) throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metrics added" });
    });
});

router.post("/delete-one", authCheck, (req: any, res: any) => {
    dbMet.deleteByTimestamp(req.session.user.username, req.body.timestamp, (err: Error | null) => {
        console.log(err);
        if (err) throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metric deleted" });
    });
});

router.post("/delete", authCheck, (req: any, res: any) => {
    dbMet.deleteById(req.session.user.username, (err: Error | null) => {
        if (err) throw err;
        res.status(200).render("message", { loggedIn: req.session.loggedIn, message: "Metrics deleted" });
    });
});

export default router;
