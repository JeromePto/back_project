import express from "express";

import { User } from "../controllers/user";
// import { dbUser } from "../server";
import { dbUser } from "../utils/dbInstances";

const router = express.Router();

router.get("/login", (req: any, res: any) => {
    if (req.session.loggedIn) {
        res.redirect("/");
    } else {
        res.render("login", { loggedIn: req.session.loggedIn });
    }
});

router.get("/signup", (req: any, res: any) => {
    if (req.session.loggedIn) {
        res.redirect("/");
    } else {
        res.render("signup", { loggedIn: req.session.loggedIn });
    }
});

router.get("/logout", (req: any, res: any) => {
    if (req.session.loggedIn) {
        delete req.session.loggedIn;
        delete req.session.user;
        res.redirect("/");
    } else {
        res.redirect("/");
    }
});

router.post("/login", (req: any, res: any, next: any) => {
    dbUser.get(req.body.username, (err: Error | null, result?: User) => {
        if (err) return next(err);
        if (
            result === undefined ||
            !result.validatePassword(req.body.password)
        ) {
            res.redirect("/login");
        } else {
            req.session.loggedIn = true;
            req.session.user = result;
            res.redirect("/");
        }
    });
});

export default router;
