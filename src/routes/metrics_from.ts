import express from "express";
import d3nLine from "d3node-linechart";

// import { dbMet } from "../server";
import { authCheck } from "../utils/authCheck";

import { dbMet } from "../utils/dbInstances";

const router = express.Router();

router.get("/add-metrics", authCheck, (req: any, res: any) => {
    res.render("add-metrics", { loggedIn: req.session.loggedIn });
});

router.get("/del-metrics", authCheck, (req: any, res: any) => {
    res.render("del-metrics", { loggedIn: req.session.loggedIn});
});

router.get("/get-metrics", authCheck, (req: any, res: any) => {
    const data = new Array();
    dbMet.get(req.session.user.username, (err: Error | null, result?: any) => {
        if (err) throw err;
        result.forEach(element => {
            data.push({ key: element.timestamp, value: element.value });
        });
        const svg = d3nLine({ data: data, isCurve: false }).svgString();
        res.render("./graph", { loggedIn: req.session.loggedIn, graph: svg });
    });
});

export default router;
