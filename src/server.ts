import express = require("express");
import path = require("path");
import fs from "fs";
import https from "https";
import bodyparser = require("body-parser");
import favicon from "serve-favicon";
import morgan from "morgan";
import session = require("express-session");
import levelSession from "level-session-store";

import { MetricsHandler } from "./controllers/metrics";
import { UserHandler } from "./controllers/user";
import { dbMet } from "./utils/dbInstances";
import { dbUser } from "./utils/dbInstances";

import userRouter from "./routes/user";
import authRouter from "./routes/auth";
import metricsRouter from "./routes/metrics";
import metricsFormRouter from "./routes/metrics_from";

const app = express();
const LevelStore = levelSession(session);
const port: string = process.env.PORT || "8080";

process.title = "node_app_process";

app.use(express.static(path.join(__dirname, "..", "public")));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(favicon(path.join(__dirname, "..", "public", "favicon.ico")));

app.set("views", __dirname + "/../views");
app.set("view engine", "ejs");

// for dev
// import { LevelDB } from "./controllers/leveldb";
// LevelDB.clear("./db/users");

app.use(morgan("dev"));

let sessions_path = path.join(".", "db", "sessions");

if (process.env.NODE_ENV === "TEST") {
    sessions_path = path.join(".", "db-test", "sessions");
}

if(!fs.existsSync(sessions_path)) {
    fs.mkdirSync(sessions_path, { recursive: true });
}

app.use(
    session({
        secret: "mon secret",
        store: new LevelStore(sessions_path),
        resave: true,
        saveUninitialized: true,
    })
);

// for dev

// app.get("/dev/req", (req: any, res: any) => {
//     res.send(req.session);
// });

// import { User } from "./user";

// dbUser.save(new User("aaa", "aaa@aaa.fr", "aaa"), err => {
//     if (err) {
//         throw err;
//     }
// });

app.use(authRouter);
app.use("/user", userRouter);
app.use(metricsFormRouter);
app.use("/metrics", metricsRouter);

app.get("/", (req: any, res: any) => {
    res.render("home", { loggedIn: req.session.loggedIn });
});

app.use((req: any, res: any, next: any) => {
    res.status(404).send("<h1>Page not found</h1>");
});

let server;
if (process.env.SECURE === "1") {
    server = https
        .createServer(
            {
                key: fs.readFileSync("secrets/server.key"),
                cert: fs.readFileSync("secrets/server.cert"),
            },
            app
        )
        .listen(port, () => {
            console.log(`Server is running on https://localhost:${port}`);
        });
} else {
    server = app.listen(port, (err: Error) => {
        if (err) throw err;
        console.log(`Server is running on http://localhost:${port}`);
    });
}

server.closeServer = (callback) => {
    server.close(() => {
        dbMet.close().then(() => {
            dbUser.close().then(() => {
                callback();
            });
        });
    })
};

const io = require("socket.io")(server);
io.on("connection", (sockerServer) => {
    sockerServer.on("npmStop", () => {
        process.exit(0);
    });
});

export { app, server };
