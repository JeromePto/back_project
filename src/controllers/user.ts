import { LevelDB } from "./leveldb";
import WriteStream from "level-ws";
import bcrypt from "bcrypt";

export class User {
    public username: string;
    public email: string;
    private password: string = "";

    constructor(
        username: string,
        email: string,
        password: string = "",
        passwordHashed: boolean = false
    ) {
        this.username = username;
        this.email = email;

        if (!passwordHashed) {
            this.setPassword(password);
        } else {
            this.password = password;
        }
    }

    static fromDb(username: string, value: any): User {
        const [password, email] = value.split(":");
        return new User(username, email, password, true);
    }

    public setPassword(toSet: string): void {
        // Hash and set password
        this.password = bcrypt.hashSync(toSet, 10);
    }

    public getPassword(): string {
        return this.password;
    }

    public validatePassword(toValidate: String): boolean {
        return bcrypt.compareSync(toValidate, this.getPassword());
    }
}

export class UserHandler {
    public db: any;

    constructor(path: string) {
        this.db = LevelDB.open(path);
    }

    public close() {
        return new Promise((resolve, reject) => {
            this.db.close(() => {
                resolve();
            });
        });   
    }

    public get(
        username: string,
        callback: (err: Error | null, result?: User) => void
    ) {
        this.db.get(`user:${username}`, function(err: any, data: any) {
            if (err) {
                if (err.notFound) {
                    return callback(null);
                }
                return callback(err);
            }
            else if (data === undefined) callback(null, data);
            callback(null, User.fromDb(username, data));
        });
    }

    public save(user: User, callback: (err: Error | null) => void) {
        // dev console.log("save: ", user);
        
        
        this.db.put(
            `user:${user.username}`,
            `${user.getPassword()}:${user.email}`,
            (err: Error | null) => {
                if (err) callback(err);
                return callback(null)
            }
        );
    }

    public delete(username: string, callback: (err: Error | null) => void) {
        this.db.del(`user:${username}`, (err: Error | null) => {
            if (err) return callback(err);
            return callback(null);
        });
    }
}
