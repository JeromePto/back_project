import { LevelDB } from "./leveldb";
import WriteStream from "level-ws";

export class Metric {
    public timestamp: string;
    public value: number;

    constructor(ts: string, v: number) {
        this.timestamp = ts;
        this.value = v;
    }
}

export class MetricsHandler {
    private db: any;

    constructor(dbPath: string) {
        this.db = LevelDB.open(dbPath);
    }

    public close() {
        return new Promise((resolve, reject) => {
            this.db.close(() => {
                resolve();
            });
        });   
    }

    public save(
        key: string,
        metrics: Metric[],
        callback: (error: Error | null) => void
    ) {
        const stream = WriteStream(this.db);
        stream.on("error", (err: Error) => {
            callback(err);
        });
        stream.on("close", () => {
            callback(null);
        });
        metrics.forEach((m: Metric) => {
            stream.write({
                key: `metric:${key}:${m.timestamp}`,
                value: m.value,
            });
        });
        stream.end();
    }

    public get(
        key: string,
        callback: (err: Error | null, result?: Metric[]) => void
    ) {
        const stream = this.db.createReadStream();
        var met: Metric[] = [];

        stream
            .on("error", callback)
            .on("data", (data: any) => {
                const [_, k, timestamp] = data.key.split(":");
                const value = data.value;
                if (key != k) {
                    // console.log(
                    //     `LevelDB error: ${data} does not match key ${key}`
                    // );
                } else {
                    met.push(new Metric(timestamp, value));
                }
            })
            .on("end", (err: Error) => {
                callback(null, met);
            });
    }

    public getAll(callback: (err: Error | null, result?: Metric[]) => void) {
        const stream = this.db.createReadStream();
        var met: Metric[] = [];

        stream
            .on("error", callback)
            .on("data", (data: any) => {
                const [_, k, timestamp] = data.key.split(":");
                const value = data.value;
                met.push(new Metric(timestamp, value));
            })
            .on("end", (err: Error) => {
                callback(null, met);
            });
    }

    public deleteById(id: string, callback: (error: Error | null) => void) {
        const stream = this.db
            .createKeyStream()
            .on("data", data => {
                if (data.split(":")[1] === id) {
                    this.db.del(data, err => {
                        if (err) throw err;
                    });
                }
            })
            .on("error", err => {
                callback(err);
            })
            .on("close", () => {
                callback(null);
            });
    }

    public deleteByTimestamp(
        id: string,
        timestamp: string,
        callback: (error: Error | null) => void
    ) {
        const stream = this.db
            .createKeyStream()
            .on("data", data => {
                if (
                    data.split(":")[1] === id &&
                    data.split(":")[2] === timestamp
                ) {
                    this.db.del(data, err => {
                        if (err) throw err;
                    });
                }
            })
            .on("error", err => {
                callback(err);
            })
            .on("close", () => {
                callback(null);
            });
    }
}
