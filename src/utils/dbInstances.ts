import path from "path";

import { MetricsHandler } from "../controllers/metrics";
import { UserHandler } from "../controllers/user";


let db_path = path.join(".", "db");

if (process.env.NODE_ENV === "TEST") {
    db_path = path.join(".", "db-test");

}

export const dbMet = new MetricsHandler(path.join(db_path, "metrics"));
export const dbUser = new UserHandler(path.join(db_path, "users"));
