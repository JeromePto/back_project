export const authCheck = (req: any, res: any, next: any) => {
    if (req.session.loggedIn) {
        next();
    } else {
        res.redirect("/");
    }
};