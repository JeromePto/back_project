import { LevelDB } from "../src/controllers/leveldb";

LevelDB.clear("./db/users");
LevelDB.clear("./db/metrics");
LevelDB.clear("./db/sessions");
LevelDB.clear("./db/test/user");
LevelDB.clear("./db/test/metrics");

console.log("DB now empty");