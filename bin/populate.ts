import { Metric, MetricsHandler } from "../src/controllers/metrics";
import { User, UserHandler } from "../src/controllers/user";

const dbMet: MetricsHandler = new MetricsHandler("./db/metrics");
const dbUser: UserHandler = new UserHandler("./db/users");

dbUser.save(new User("aaa", "aaa@aaa.fr", "aaa"), err => {
    if (err) throw err;
    var metrics1 = new Array();
    metrics1.push(new Metric("1", 12));
    metrics1.push(new Metric("2", 40));
    metrics1.push(new Metric("3", 24));
    dbMet.save("aaa", metrics1, (err: Error | null) => {
        if (err) throw err;
        dbUser.save(new User("bbb", "bbb@bbb.fr", "bbb"), err => {
            if (err) throw err;
            var metrics2 = new Array();
            metrics2.push(new Metric("1", 6));
            metrics2.push(new Metric("2", 15));
            metrics2.push(new Metric("3", -3));
            dbMet.save("bbb", metrics2, (err: Error | null) => {
                if (err) throw err;
                return console.log("OK");
            });
        });
    });
});
